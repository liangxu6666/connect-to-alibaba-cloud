#ifndef __CAT1_H
#define __CAT1_H

/****************************头文件包含******************************************************/
#include "stm32f10x.h"
#include "bsp_usart.h"
#include "bsp_SysTick.h"
#include "mqtt.h"
#include "bsp_led.h"
/****************************宏定义、宏函数******************************************************/
/*有人模组：串口通信引脚*/
#define  CAT1_USARTx                   USART2
#define  CAT1_USART_CLK                RCC_APB1Periph_USART2
#define  CAT1_USART_APBxClkCmd         RCC_APB1PeriphClockCmd
#define  CAT1_USART_BAUDRATE           115200

#define  CAT1_USART_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  CAT1_USART_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
    
#define  CAT1_USART_TX_GPIO_PORT        GPIOA   /* 串口2 连接有人Cat1芯片引脚6、7   */
#define  CAT1_USART_TX_GPIO_PIN         GPIO_Pin_2
#define  CAT1_USART_RX_GPIO_PORT       	GPIOA
#define  CAT1_USART_RX_GPIO_PIN        	GPIO_Pin_3

#define  CAT1_USART_IRQ                	USART2_IRQn
#define  CAT1_USART_IRQHandler         	USART2_IRQHandler


/*有人模组：功能引脚*/
/*功能引脚:link   判断link A、B连接状态*/
#define  CAT1_LINK_CLK 	    			(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO)		/* LINK GPIO端口时钟(stm32GPIO中断依赖AFIO)*/
#define	 CAT1_LINKA_GPIO_PORT			GPIOB		
#define  CAT1_LINKA_GPIO_PIN         	GPIO_Pin_13  //指示第一路网络连接是否建立，建立后输出高电平，未建立连接时输出低电平
#define  CAT1_LINKA_EXTI_LINE			EXTI_Line13	//中断线，与GPIO_PIN一致
#define  CAT1_LINKA_INT_EXTI_IRQ		EXTI15_10_IRQn	//中断号。
#define  CAT1_LINKA_PortSourceGPIOx		GPIO_PortSourceGPIOB
#define  CAT1_LINKA_PinSourcex			GPIO_PinSource13
#define  CAT1_LINKA_IRQHandler			EXTI15_10_IRQHandler  //中断服务函数

//#define  CAT1_LINKB_GPIO_PORT			GPIOx
//#define  CAT1_LINKB_GPIO_PIN			GPIO_Pin_x  //指示第二路网络连接是否建立，建立后输出高电平，未建立连接时输出低电平

/*功能引脚:POWERKEY  控制模块开关机*/
#define  CAT1_POWERKEY_CLK 	    		RCC_APB2Periph_GPIOB		/* POWERKEY GPIO端口时钟 */
#define  CAT1_POWERKEY_GPIO_PORT		GPIOB
#define  CAT1_POWERKEY_GPIO_PIN			GPIO_Pin_5  //模块开关机，低电平关机。
#define  CAT1_POWERKEY_ON				CAT1_POWERKEY_OFF;Cat1_delay_Ms(1000);\
										GPIO_SetBits(CAT1_POWERKEY_GPIO_PORT,CAT1_POWERKEY_GPIO_PIN)   //开机
#define  CAT1_POWERKEY_OFF				GPIO_ResetBits(CAT1_POWERKEY_GPIO_PORT,CAT1_POWERKEY_GPIO_PIN)   //关机

/*功能引脚:RESET  模块重启复位，低电平有效。*/
#define  CAT1_RESETKEY_CLK 	    		RCC_APB2Periph_GPIOB		/* POWERKEY GPIO端口时钟 */
#define  CAT1_RESETKEY_GPIO_PORT		GPIOB
#define  CAT1_RESETKEY_GPIO_PIN			GPIO_Pin_15  //模块开关机，低电平关机。
#define  CAT1_RESET						GPIO_ResetBits(CAT1_RESETKEY_GPIO_PORT,CAT1_RESETKEY_GPIO_PIN);\
										Cat1_delay_Ms(1000);\
										GPIO_SetBits(CAT1_RESETKEY_GPIO_PORT,CAT1_RESETKEY_GPIO_PIN)  //重置

/***********************物联网地址宏定义*****************************/
#define LOCAL_IP		"8553kf2129.vicp.fun"	
#define LOCAL_PORT		58321
#define ALI_CLOUD_IP	"iot-06z00f4odqq9hvx.mqtt.iothub.aliyuncs.com"
#define ALI_CLOUD_PORT	1883

/***********************常量宏定义*****************************/
#define BUFFLEN 2048

/*****************************结构体定义**************************************/
typedef struct Buffstruct{
	uint8_t flag;//0不可读,可写；1可读，不可写；
	uint8_t buff[BUFFLEN];
	uint16_t len;
}Buff; 

typedef struct Cat1ConnectState
{
	uint8_t LinkA_TCP_ConnectFlag;		//linkA 的TCP层连接状态，0x01为linkA连接，0x02为linkB连接，0x04为linkC连接......
	uint8_t	LinkA_MQTT_ConnectFlag;		//物联网平台应用层连接状态，0x01为linkA连接（处于MQTT协议透传阶段）
	uint8_t	LinkA_OTA_EventFlag;			//linkA 的 OTA事件状态（处于版本更新的数据传输阶段）
}ConnectState;
extern ConnectState Cat1_State;
extern Buff Cat1_Buff;


void Buff_init(Buff* mybuff);
void Buff_write(Buff* mybuff,uint8_t ch);
void Buff_write_end(Buff* mybuff);
void Buff_read(Buff* mybuff);
void Buff_read_end(Buff* mybuff);
/*****************************公用函数**************************************/
//cat模组应用函数
void Cat1_Init(void);//系统初始化
uint8_t Cat1_Wait_Reset(uint8_t waitTime);//等待重置
uint8_t Cat1_AT_Test(void);
uint8_t Cat1_Wait_Recv(const char* data, uint8_t dataLen, uint32_t timeoutMs);//等待回复
uint8_t Cat1_LinkA_Connect(const char* IP, int port);//连接并开启
uint8_t Cat1_Heart_Config(uint16_t cycleSeconds, const char* symbol);//心跳
uint8_t Cat1_Config_Save(void);//保存配置
uint8_t Cat1_Config_Exit(void);//退出配置模式
uint8_t Cat1_Reset(void);
//发送字符：
void Cat1_USARTx_SendByte( uint8_t ch);
void Cat1_USARTx_SendString( char *str);
void Cat1_USARTx_SendHalfWord( uint16_t ch);
void Cat1_USARTx_printf( const char* fmt, ...);
//中断服务函数：
void CAT1_USART_IRQHandler(void);
void CAT1_LINKA_IRQHandler(void);



#endif
