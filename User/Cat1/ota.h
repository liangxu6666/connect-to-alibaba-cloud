#ifndef OTA_H
#define OTA_H
#include <stdint.h>
#include <string.h>
#include "stm32f10x.h"
/************************************宏定义*************************************/
/*Falsh参数*/
#define STM32_FLASH_SADDR  0x08000000   //stm32f103VE的flash起始地址
#define STM32_PAGE_SIZE    2048 	//stm32f103VE的flash每个页数的大小(一个扇区2Kb 即 2*1024 或 0x00000800 )
#define STM32_PAGE_NUM     256          //stm32的flash的page个数（sm32f103VE为0~255页）

/*BootLoader区参数*/
#define STM32_B_PAGE_NUM   20			//stm32的B区的分的page个数(修改本程序的 IROM Size = 20*2*1024 = 0xA000)

/*UserPama用户参数区*/
//内容：
#define OTA_SET_FLAG		0x12341234  //OTA标志,如果为此值，则跳转B区更新固件，否则进入A区

#define STM32_U_PAGE_NUM   1			//stm32的U区的分的page个数
#define STM32_U_SPAGE		STM32_B_PAGE_NUM  //UserPama用户参数区的首扇区
#define STM32_U_SADDR		(STM32_FLASH_SADDR+STM32_U_SPAGE*STM32_PAGE_SIZE)  //UserPama用户参数区的首地址

#define OTA_INFO_SADDR		STM32_U_SADDR  //赋予OTA_Info结构体的数据的首地址（其实就是用户参数区首地址）
#define OTA_INFOCB_SIZE		sizeof(OTA_Info)  //OTA_Info结构体的大小
	
#define User_Pama_SADDR		(STM32_U_SADDR + OTA_INFOCB_SIZE)   //赋予User_Pama结构体的数据的首地址

/*文件系统路径名称*/
#define UPDATE_DIR_PATH "0:/VERSION/%8s.bin"

/************************************结构体定义*************************************/
//OTA结构体，存储user参数信息，每个成员必须为uint32_t类型
typedef struct{
	uint32_t OTA_flag;			//OTA标志
    uint32_t LoadLen[2];		//下载长度(记录不同程序文件的长度) 0号成员为OTA程序大小
	uint32_t OTA_Version[8];	//当前OTA版本号（前8个字节为当前版本号放入[0][1]，后8个字节为要更新的版本号放入[2][3]）版本号名规则：V年月日号（V240116a）
}OTA_InfoCB;



/************************************全局变量声明*************************************/
extern OTA_InfoCB OTA_Info;        //OTA信息结构体












/************************************函数声明*************************************/
void OTAInfo_Read(void);
void OTA_INFO_SADDR_Write(OTA_InfoCB* OTAInfo);
void OTA_Version_Modify(const char* versionNameNow,const char* versionNameFuture);


static int str2int(char* str, uint8_t strlen);
static void int2mystr(char* str, uint8_t strlen,int uintdata);
#endif	/* OTA_H */
