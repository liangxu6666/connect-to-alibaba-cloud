#include "ota.h"
#include "bsp_usart.h"

/************************************全局变量*************************************/
OTA_InfoCB OTA_Info;        //OTA信息结构体


/************************************函数定义*************************************/
/**
  * @brief  将falsh中参数区的数据读到OTA_Info结构体中
  * @param  void
  * @retval void 
  */
void OTAInfo_Read(void)
{
   memcpy(&OTA_Info,(void*)OTA_INFO_SADDR,OTA_INFOCB_SIZE);
}


/**
  * @brief  将某打包好的OTA_InfoCB结构体数据放到falsh上的OTA_INFO_SADDR地址
  * @param  OTA_InfoCB*
  * @retval void 
  */
void OTA_INFO_SADDR_Write(OTA_InfoCB* OTAInfo)
{ 
	uint32_t Address = STM32_U_SADDR;
	uint16_t size = OTA_INFOCB_SIZE;
	uint32_t* p = (uint32_t*)OTAInfo;
	FLASH_Unlock();//Flash 写入解锁
	FLASH_ErasePage(STM32_U_SADDR);
	while(size>0)
	{
		FLASH_ProgramWord(Address, *p);
		p+=1;
		Address+=4;
		size-=4; 
	}
	FLASH_Lock();
}


/**
  * @brief  修改flash参数区的版本号
  * @param  versionNameNow当前版本号，versionNameFuture要更新的版本号（可为NULL）
  * @retval void 
  */
void OTA_Version_Modify(const char* versionNameNow,const char* versionNameFuture)
{
	OTAInfo_Read();
	
	if(versionNameNow != NULL && strlen(versionNameNow)<=9)
	{
		memcpy((uint8_t *)OTA_Info.OTA_Version,versionNameNow,9);
	}
	
	if(versionNameFuture != NULL && strlen(versionNameFuture)<=9)
	{
		memcpy((uint8_t *)&OTA_Info.OTA_Version[4],versionNameFuture,9);
		OTA_Info.OTA_flag = OTA_SET_FLAG;
	}
	
	OTA_INFO_SADDR_Write(&OTA_Info);//写入flash中
}


/**
  * @brief  将str字符串转为uint16_t
  */
static int str2int(char* str, uint8_t strlen)
{
	int result = 0;
	for (uint8_t i = 0; i < strlen; i++)
	{
		if (str[i] >= '0' && str[i] <= '9')
		{
			uint8_t mid = str[i] - '0';
			result = result * 10 + mid;
		}
		else
		{
			printf("字符串转int错误!\n");
			return 0;
		}
	}
	return result;
}

/**
  * @brief  将uint16_t字符串转为字符串,字符串小于6，则补‘0’
  */
static void int2mystr(char* str, uint8_t strlen,int uintdata)
{
	for (int i = strlen-1; i >=0 ; --i)
	{
			str[i] = uintdata % 10 + '0';
			uintdata /= 10;
	}

	str[strlen] = '\0';
}





