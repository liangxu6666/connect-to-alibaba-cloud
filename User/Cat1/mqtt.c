#include "mqtt.h"
#include "string.h"
#include "bsp_usart.h"
#include "bsp_cat1.h"
#include "ota.h"
#include "bsp_SysTick.h"
#include "file_system.h"

MQTT_CB Ali_mqtt;


//私有函数：
static void Message_Test(uint16_t Len);
static void Version_Pack(char * temporaryBuff);
static void MQTT_Delay(uint16_t Ms);

/**
  * @brief  mqtt延迟函数
  * @param  Ms毫秒
  */
static void MQTT_Delay(uint16_t Ms)
{
	SysTick_Delay_Ms(Ms);
}

/**
  * @brief  mqtt报文打包后打印到串口测试
  * @param  len长度
  */
static void Message_Test(uint16_t len)
{
	printf("\r\n************************发送报文内容:************************\r\n");
	printf("ASCII版本:\r\n");
	for(uint16_t i=0;i<len;++i)
		Usart_SendByte(USART1,Ali_mqtt.Pack_buff[i]);
	printf("\r\n16进制版本:\r\n");
	for(uint16_t i=0;i<len;++i)
		printf("%x",Ali_mqtt.Pack_buff[i]);
	printf("\r\n*************************************************************\r\n");
}

static void Pack_Send(uint16_t Len)
{
	for(uint16_t i = 0;i<Len;++i)
		Cat1_USARTx_SendByte(Ali_mqtt.Pack_buff[i]);
}
	

/**
  * @brief  将版本号打包为json格式
	
  */
static void Version_Pack(char * temporaryBuff)
{
	char versionName[10]={0};
	OTAInfo_Read();
	memcpy( versionName, (uint8_t *)OTA_Info.OTA_Version,8);
	
	sprintf(temporaryBuff,"{\"id\": 1,\"params\": {\"version\": \"%s\"}}",versionName);
}

 /**
  * @brief  mqtt连接报文
  */
void Ali_MQTT_ConnectPack(void)
{
	/*初始化*/
	Ali_mqtt.MessageID = 1;
	Ali_mqtt.Fixed_len = 1;
	Ali_mqtt.Variable_len = 10;
	Ali_mqtt.Payload_len = 2 + strlen(CLIENT_ID) + 2 + strlen(USER_NAME) + 2 + strlen(PASSWORD);
	Ali_mqtt.Remaining_len = Ali_mqtt.Variable_len + Ali_mqtt.Payload_len;//剩余长度字段= 可变报头的长度+有效载荷的长度。
	
	/*****************一、固定报头:报文类型与指定控制报文的标志位***********************************/
		/*1.报文类型; 2.指定控制报文的标志位*/
	Ali_mqtt.Pack_buff[0] = 0x10;//报文类型（1），保留位（0）
	
	/*	3.剩余长度
		规则：低 7 位有效位用于编码数据，最高有效位用于指示是否有更多的字节。*/
	
	do{
		if(Ali_mqtt.Remaining_len/128 == 0) //剩余长度小于128
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = Ali_mqtt.Remaining_len;
		}
		else
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = (Ali_mqtt.Remaining_len % 128) | 0x80;//进位 DEC_128 = HEX_80 = BIN_10000000 
		}
		Ali_mqtt.Fixed_len ++;
		Ali_mqtt.Remaining_len = Ali_mqtt.Remaining_len/128;
	}while(Ali_mqtt.Remaining_len);
	
	/****************************二、可变报头**********************************/
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+0] = 0x00;	//协议版本号(长度MSB)
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+1] = 0x04;	//协议版本号(长度LSB)
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+2] = 0x4D;	//'M'
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+3] = 0x51;	//'Q'
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+4] = 0x54;	//'T'
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+5] = 0x54;	//'T'
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+6] = 0x04;	//协议级别				1100 0010
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+7] = 0xC2;	//标志位bit:(7)用户名;(6)密码;(5)遗嘱保留;(4)(3)遗嘱QoS等级;(2)遗嘱开关;(1)清理会话;(0)保留位;
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+8] = 0x00;	//保持连接 MSB(一个以秒为单位的时间间隔)
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+9] = 0x64;	//保持连接 LSB(100s即为0x64)
	
	/**************************三、负载消息体*******************************/
	/*顺序：客户端标识符，遗嘱主题，遗嘱消息，用户名，密码*/
	uint16_t index = Ali_mqtt.Fixed_len+10;
	Ali_mqtt.Pack_buff[index++] = strlen(CLIENT_ID)/256;	//客户端标识符长度 MSB
	Ali_mqtt.Pack_buff[index++] = strlen(CLIENT_ID)%256;	//客户端标识符长度 LSB
	memcpy(&Ali_mqtt.Pack_buff[index] ,CLIENT_ID, strlen(CLIENT_ID));//客户端标识符
	
	index += strlen(CLIENT_ID);
	Ali_mqtt.Pack_buff[index++] = strlen(USER_NAME)/256;	//用户名长度 MSB
	Ali_mqtt.Pack_buff[index++] = strlen(USER_NAME)%256;	//用户名长度 LSB
	memcpy(&Ali_mqtt.Pack_buff[index] ,USER_NAME, strlen(USER_NAME));//用户名
	
	index += strlen(USER_NAME);
	Ali_mqtt.Pack_buff[index++] = strlen(PASSWORD)/256;	//密码长度 MSB
	Ali_mqtt.Pack_buff[index++] = strlen(PASSWORD)%256;	//密码长度 LSB
	memcpy(&Ali_mqtt.Pack_buff[index] ,PASSWORD, strlen(PASSWORD));//密码
	
	index += strlen(PASSWORD);
	
	#if MQTT_DEBUG 
		Message_Test(index);
	#endif
	
	
	Pack_Send(index);
}

 /**
  * @brief  mqtt订阅报文
  * @param  topic订阅主题
  */
void Ali_MQTT_SubcribePack(char* topic)
{
	Ali_mqtt.Fixed_len = 1;
	Ali_mqtt.Variable_len = 2;
	Ali_mqtt.Payload_len = 2 + strlen(topic) + 1;
	Ali_mqtt.Remaining_len = Ali_mqtt.Variable_len + Ali_mqtt.Payload_len; //剩余长度字段= 可变报头的长度+有效载荷的长度。
	
	/*****************一、固定报头:SUBSCRIBE 报文用于创建一个或多个订阅*********************/
	/*1.报文类型; 2.指定控制报文的标志位*/
	Ali_mqtt.Pack_buff[0] = 0x82;
	
	/*	3.剩余长度*/
	do{
		if(Ali_mqtt.Remaining_len/128 == 0) //剩余长度小于128
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = Ali_mqtt.Remaining_len;
		}
		else
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = (Ali_mqtt.Remaining_len % 128) | 0x80;//进位 DEC_128 = HEX_80 = BIN_10000000 
		}
		Ali_mqtt.Fixed_len ++;
		Ali_mqtt.Remaining_len = Ali_mqtt.Remaining_len/128;
	}while(Ali_mqtt.Remaining_len);
	
	/****************************二、可变报头**********************************/
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+0] = Ali_mqtt.MessageID / 256;	//报文标识符 MSB (0)用于在通信协议中唯一标识一个消息或报文。
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+1] = Ali_mqtt.MessageID % 256;	//报文标识符 LSB (10)如果接收到具有相同报文标识符的消息，接收方可以根据需要忽略重复的消息或者进行相应的处理。
	Ali_mqtt.MessageID++;//独一无二的标识符
	
	/****************************三、负载**********************************/
	/*1.主题过滤器*/
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+2] = strlen(topic) / 256;	//topic长度 MSB
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+3] = strlen(topic) % 256;	//topic长度 LSB
	memcpy(&Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+4],topic,strlen(topic));
	
	/*2.服务质量要求*/
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+4+strlen(topic)] = 0;
	
	
	Pack_Send(Ali_mqtt.Fixed_len+Ali_mqtt.Variable_len+Ali_mqtt.Payload_len);
}

 /**
  * @brief  mqtt设备发布Publish报文（Qs0级别，无标识符）
  * @param  topic主题，data数据
  */
void Ali_MQTT_PublishQs0(const char* topic ,const char * data)
{
	Ali_mqtt.Fixed_len = 1;
	Ali_mqtt.Variable_len = 2 + strlen(topic);//主题长度数值大小（2字节）+ 主题长度
	Ali_mqtt.Payload_len = strlen(data);
	Ali_mqtt.Remaining_len = Ali_mqtt.Variable_len + Ali_mqtt.Payload_len; //剩余长度字段= 可变报头的长度+有效载荷的长度。
	
	/*****************一、固定报头: Publish报文用于设备向服务端的某主题发送应用消息*********************/
	/*1.报文类型(3); 2.指定控制报文的标志位:DUP(0);QoS 等级(00);RETAIN(0)*/
	Ali_mqtt.Pack_buff[0] = 0x30;
	
	/*	3.剩余长度*/
	do{
		if(Ali_mqtt.Remaining_len/128 == 0) //剩余长度小于128
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = Ali_mqtt.Remaining_len;
		}
		else
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = (Ali_mqtt.Remaining_len % 128) | 0x80;//进位 DEC_128 = HEX_80 = BIN_10000000 
		}
		Ali_mqtt.Fixed_len ++;
		Ali_mqtt.Remaining_len = Ali_mqtt.Remaining_len/128;
	}while(Ali_mqtt.Remaining_len);
	
	/*****************二、可变报头:***************************************************************/
	/*	1.topic长度*/
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+0] = strlen(topic)/256;	//Length MSB 
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+1] = strlen(topic)%256;	//Length LSB
	memcpy(&Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+2],topic,strlen(topic));	//topic主题
	
	/*****************三、负载:***************************************************************/
	memcpy(&Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+2+strlen(topic)],data,strlen(data));		//data数据
	
	Pack_Send(Ali_mqtt.Fixed_len +Ali_mqtt.Payload_len+Ali_mqtt.Variable_len);	//发送数据
	//Message_Test(Ali_mqtt.Fixed_len + Ali_mqtt.Payload_len+Ali_mqtt.Variable_len);
}


 /**
  * @brief  mqtt设备发布Publish报文（Qs1级别，有报文标识符）
  * @param  topic主题，data数据
  */
void Ali_MQTT_PublishQs1(const char* topic ,const char * data)
{
	Ali_mqtt.Fixed_len = 1;
	Ali_mqtt.Variable_len = 2 + strlen(topic) + 2;//主题长度数值大小（2字节）+ 主题长度 + 报文标识符（2字节）
	Ali_mqtt.Payload_len = strlen(data);
	Ali_mqtt.Remaining_len = Ali_mqtt.Variable_len + Ali_mqtt.Payload_len; //剩余长度字段= 可变报头的长度+有效载荷的长度。
	
	/*****************一、固定报头: Publish报文用于设备向服务端的某主题发送应用消息*********************/
	/*1.报文类型(3); 2.指定控制报文的标志位:DUP(0);QoS 等级(01);RETAIN(0)*/
	Ali_mqtt.Pack_buff[0] = 0x32;
	
	/*	3.剩余长度*/
	do{
		if(Ali_mqtt.Remaining_len/128 == 0) //剩余长度小于128
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = Ali_mqtt.Remaining_len;
		}
		else
		{
			Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len] = (Ali_mqtt.Remaining_len % 128) | 0x80;//进位 DEC_128 = HEX_80 = BIN_10000000 
		}
		Ali_mqtt.Fixed_len ++;
		Ali_mqtt.Remaining_len = Ali_mqtt.Remaining_len/128;
	}while(Ali_mqtt.Remaining_len);
	
	/*****************二、可变报头:***************************************************************/
	/*	1.topic长度*/
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+0] = strlen(topic)/256;	//Length MSB 
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+1] = strlen(topic)%256;	//Length LSB
	memcpy(&Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+2],topic,strlen(topic));	//topic主题
	
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+2+strlen(topic)] = Ali_mqtt.MessageID/256;	//报文标识符 MSB 
	Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+3+strlen(topic)] = Ali_mqtt.MessageID%256;	//报文标识符 LSB
	Ali_mqtt.MessageID++;
	/*****************三、负载:***************************************************************/
	memcpy(&Ali_mqtt.Pack_buff[Ali_mqtt.Fixed_len+4+strlen(topic)],data,strlen(data));		//data数据
	
	Pack_Send(Ali_mqtt.Fixed_len +Ali_mqtt.Payload_len+Ali_mqtt.Variable_len);	//发送数据
	//Message_Test(Ali_mqtt.Fixed_len + Ali_mqtt.Payload_len+Ali_mqtt.Variable_len);
}

 /**
  * @brief  mqtt回复报文确认
  * @param  数据、数据长度
  */
void Ali_MQTT_Received(uint8_t* data, uint16_t len)
{
	
	if((len == 4) && (data[0] == 0x20))	//收到CONNACK – 确认连接请求
	{
		if(data[3] == 0x00)
		{
			printf("Connect报文连接服务器成功\n");
			Cat1_State.LinkA_MQTT_ConnectFlag |= 0x01;//成功连接服务器
			
			//订阅主题
			Ali_MQTT_SubcribePack(DEVICE_PROPERTY_SET_TOPIC); //订阅属性设置主题
			Ali_MQTT_SubcribePack(CLOUD_DOWNLOAD_REPLY_TOPIC); //订阅下载响应主题
			
			
			
			//发送版本号报文
			char temporaryBuff[64]={0};
			Version_Pack(temporaryBuff);
			Ali_MQTT_PublishQs1(DEVICE_VERSION_POST_TOPIC,temporaryBuff);
			printf("版本号发布(标识符:%d)\n",Ali_mqtt.MessageID-1);
		}
		else
		{
			printf("连接报文连接失败，准备重启\n");
			NVIC_SystemReset();
		}
	}
	else if((len == 5) && (data[0] == 0x90))	//收到SUBACK – 订阅确认
	{
		if(data[len-1] == 0x00 || data[len-1] == 0x01 )
		{
			printf("订阅成功\n");
			
		}
		else
		{
			printf("订阅失败，准备重启\n");
			NVIC_SystemReset();
		}
	}
	else if((len == 2) && (data[0] == 0xD0))	//收到心跳回复D0 00
	{
		if(data[1] == 0x00)
		{
			printf("心跳保活，连接状态\n");
			Cat1_State.LinkA_MQTT_ConnectFlag |= 0x01;//连接服务器状态
		}
		else
		{
			printf("心跳失活，准备重启\n");
			NVIC_SystemReset();
		}
	}
	else if((Cat1_State.LinkA_MQTT_ConnectFlag & 0x01) && (data[0] == 0x30))	//收到等级为0的Publish报文，处理它
	{
		Ali_MQTT_Publish_Deal(data,len);//处理云端发布的消息
	}
	else if((len == 4) && (data[0] == 0x40))	//收到等级为1的Publish报文的ACK回复
	{
		printf("发布报文被云端接收(标识符:%d)\n",data[3]);
	}
	else//未知消息，打印到DEBUG串口
	{
		printf("\r\n************************未知消息:************************\r\n");
		printf("ASCII版本:\r\n");
		for(uint16_t i=0;i<len;++i)
			Usart_SendByte(USART1,data[i]);
		printf("\r\n16进制版本:\r\n");
		for(uint16_t i=0;i<len;++i)
			printf("%x",data[i]);
		printf("\r\n*************************消息长度:%d*************************\r\n",len);
	}
}

/**
* @brief  处理 服务器发送给设备的mqtt-Publish报文
  * @param  数据、数据长度
  */
void Ali_MQTT_Publish_Deal(uint8_t* data, uint16_t len)
{
	//判断剩余长度占用了几个字节，剩余长度是固定报头的一部分，从第一个报文类型之后就是剩余长度
	uint8_t i;//下标
	for(i = 1;i<5;++i)//MQTT协议中，剩余长度下标为1，最多占用4个字节
	{
		if((data[i] & 0x80) == 0) //最高有效位1则有更多的字节，最高有效位为0则结束。
			break;
	}
	
	memset(Ali_mqtt.CMD_buff,0,PACK_BUFF_LEN);
	memcpy(Ali_mqtt.CMD_buff,&data[1+i+2],PACK_BUFF_LEN-1-2-i);//1为报文类型大小，i为剩余长度大小，2为topic长度大小
	
	if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch1\":0}"))	//云端开关量命令
	{
		printf("服务器命令开关1关闭\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch1\":0}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch1\":1}"))	//云端开关量命令
	{
		printf("服务器命令开关1打开\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch1\":1}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch2\":0}"))	//云端开关量命令
	{
		printf("服务器命令开关2关闭\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch2\":0}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch2\":1}"))	//云端开关量命令
	{
		printf("服务器命令开关2打开\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch2\":1}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch3\":0}"))	//云端开关量命令
	{
		printf("服务器命令开关3关闭\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch3\":0}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch3\":1}"))	//云端开关量命令
	{
		printf("服务器命令开关3打开\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch3\":1}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch4\":0}"))	//云端开关量命令
	{
		printf("服务器命令开关4关闭\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch4\":0}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"{\"Switch4\":1}"))	//云端开关量命令
	{
		printf("服务器命令开关4打开\n");
		Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"Switch4\":1}}");//回复
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"/ota/device/upgrade/a1P2N3Qwc77/DTU_001"))	//云端OTA命令upgrade升级（设备发布旧版本号后，云端会自动推送新版本信息）
	{
		if(sscanf((char*)Ali_mqtt.CMD_buff,OTA_UPGRADE_INFO,&Ali_mqtt.size,&Ali_mqtt.streamID,Ali_mqtt.tempVersionNameBuff) == 3) //从数据中提取三个数据
		{
			printf("\r\n*********云端推送升级包信息********\r\n");
			printf("****固件总大小:%db\r\n",Ali_mqtt.size);
			printf("****固件ID:%d\r\n",Ali_mqtt.streamID);
			printf("****固件版本号:%s\r\n",Ali_mqtt.tempVersionNameBuff);
			printf("**************进入OTA升级阶段**********\r\n");
			
			//进入OTA升级阶段
			Cat1_State.LinkA_OTA_EventFlag |= 0x01;	
			
			//合成路径/文件
			memset(Ali_mqtt.pathFileName,0,sizeof(Ali_mqtt.pathFileName));
			sprintf(Ali_mqtt.pathFileName,UPDATE_DIR_PATH,Ali_mqtt.tempVersionNameBuff);
			if(0==flie_bin_create(Ali_mqtt.pathFileName))
				printf("创建%s文件\n",Ali_mqtt.pathFileName);//将之后的数据放入此文件中
			
			//计算传输次数
			if(Ali_mqtt.size % DOWNLOADE_SIZE == 0)
				Ali_mqtt.counter = Ali_mqtt.size / DOWNLOADE_SIZE;
			else
				Ali_mqtt.counter = Ali_mqtt.size / DOWNLOADE_SIZE + 1;
			Ali_mqtt.num =1;//开始第一次
			
			printf("即将下载，下载次数：%d\n",Ali_mqtt.counter);
			Ali_mqtt.downloadLen = DOWNLOADE_SIZE;
			OTA_DownLoad(Ali_mqtt.downloadLen,(Ali_mqtt.num-1)*DOWNLOADE_SIZE);
		}
		else
		{
			printf("云端推送升级包信息格式不匹配\n");
		}
		
	}
	else if(strstr((char*)Ali_mqtt.CMD_buff,"/sys/a1P2N3Qwc77/DTU_001/thing/file/download_reply"))	//云端发送的下载回复(分片数据是此回复之后的附着)
	{
//		uint16_t i=0;
//		printf("\r\n************************云端传输下载数据:************************\r\n");
//		printf("JASON版本:\r\n");
//		for(i=0;i<len;++i)
//			Usart_SendByte(USART1,Ali_mqtt.CMD_buff[i]);
//		printf("\r\n十六进制版本:\r\n");
//		for(i=0;i<len;++i)
//			printf("%x ",data[i]);
//		printf("\r\n************************消息长度:%d*******************************\r\n",i);
		
		
		uint16_t startIndex = len - 2 - Ali_mqtt.downloadLen;	// 起始位置 = 数据总长度 - crc校验位（2位） - 下载长度
		flie_bin_write(Ali_mqtt.pathFileName , Ali_mqtt.downloadLen , &data[startIndex]);	//写入文件内
		Ali_mqtt.num++;
		if(Ali_mqtt.num<Ali_mqtt.counter)//如果不是最后一次下载
		{
			OTA_DownLoad(DOWNLOADE_SIZE,(Ali_mqtt.num-1)*DOWNLOADE_SIZE);
		}
		else if(Ali_mqtt.num == Ali_mqtt.counter)//如果是最后一次下载
		{
			Ali_mqtt.downloadLen =  Ali_mqtt.size%256;
			OTA_DownLoad(Ali_mqtt.size%256,(Ali_mqtt.num-1)*DOWNLOADE_SIZE);//将小尾巴放进去
		}
		else//下载完成
		{
			printf("云端推送的程序包下载完成\n");
			OTA_Over();
		}
	}
	else
	{
		uint16_t i=0;
		printf("\r\n************************未知云端命令:************************\r\n");
		printf("JASON版本:\r\n");
		for(i=0;i<strlen((char*)Ali_mqtt.CMD_buff);++i)
			Usart_SendByte(USART1,Ali_mqtt.CMD_buff[i]);
		printf("\r\n十六进制版本:\r\n");
		for(i=0;i<len;++i)
			printf("%x ",data[i]);
		printf("\r\n************************消息长度:%d***********************\r\n",i);
	}
}

/**
* @brief  OTA事件阶段，下载数据
 * @param  size每次下载大小、数据长度，offset偏移量
  */
void OTA_DownLoad(uint16_t size, int offset)
{
	memset(Ali_mqtt.OTA_buff,0,DOWNLOADE_SIZE);
	sprintf((char*)Ali_mqtt.OTA_buff,OTA_DOWNLOADE_REPLY,Ali_mqtt.streamID,size,offset);
	printf("下载第%d次,(进度:%2f)\n",Ali_mqtt.num,(float)offset/(float)Ali_mqtt.size);
	MQTT_Delay(300);//免费阿里云，mqtt有发布限制，一秒内最多5次发布
	Ali_MQTT_PublishQs0(DEVICE_DOWNLOAD_REQUEST_TOPIC,(char*)Ali_mqtt.OTA_buff);//设备向云端发起文件下载请求(需要提前订阅云端的下载请求响应)

}


/**
* @brief  OTA事件结束
 * @param  size每次下载大小、数据长度，offset偏移量
  */
void OTA_Over(void)
{
	Cat1_State.LinkA_OTA_EventFlag &= ~(0x01);
	
	OTAInfo_Read();//读取：
	OTA_Info.OTA_flag = OTA_SET_FLAG;//修改
	memcpy((char*)&OTA_Info.OTA_Version[2],(char*)Ali_mqtt.tempVersionNameBuff,8);//修改
	OTA_INFO_SADDR_Write(&OTA_Info);//写入参数区
	
	NVIC_SystemReset();//重启
}