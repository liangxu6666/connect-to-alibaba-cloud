#ifndef MQTT_H
#define MQTT_H

#include "stm32f10x.h"

/****************************  常量宏定义 *********************************/
#define CLIENT_ID	"a1P2N3Qwc77.DTU_001|securemode=2,signmethod=hmacsha256,timestamp=1705458222046|"	//阿里云--账户ID号
#define USER_NAME	"DTU_001&a1P2N3Qwc77"																//阿里云--用户名
#define PASSWORD	"85f7b53c9245e3da16e96688e3d3f6882d365af9046db253162bf1b9af7ee235"					//阿里云--密码
#define	HOST_URL	"a1P2N3Qwc77.iot-as-mqtt.cn-shanghai.aliyuncs.com"									//阿里云--IP
#define	PORT		1883																				//阿里云--端口号

#define	DEVICE_PROPERTY_SET_TOPIC		"/sys/a1P2N3Qwc77/DTU_001/thing/service/property/set"			//Topic类--设备属性设置
#define DEVICE_PROPERTY_POST_TOPIC		"/sys/a1P2N3Qwc77/DTU_001/thing/event/property/post"			//Topic类--设备属性上报
#define DEVICE_VERSION_POST_TOPIC		"/ota/device/inform/a1P2N3Qwc77/DTU_001"						//Topic类--设备上报固件升级信息
#define DEVICE_UPGRADE_DOWNLINK_TOPIC	"/ota/device/upgrade/a1P2N3Qwc77/DTU_001"						//Topic类--固件升级信息下行
#define CLOUD_DOWNLOAD_REPLY_TOPIC		"/sys/a1P2N3Qwc77/DTU_001/thing/file/download_reply"			//Topic类--云端发送文件分片下载请求的响应
#define	DEVICE_DOWNLOAD_REQUEST_TOPIC	"/sys/a1P2N3Qwc77/DTU_001/thing/file/download"					//Topic类--设备向云端发起分片文件下载请求

/*云端推送升级包信息，匹配格式*/
#define	OTA_UPGRADE_INFO				"/ota/device/upgrade/a1P2N3Qwc77/DTU_001	\
										{\"code\":\"1000\",\"data\":	\
										{\"size\":%d,\"streamId\":%d,\"sign\":\"%*32s\",\"dProtocol\":\"mqtt\",	\
										\"version\":\"%8s\",\"signMethod\":\"Md5\",\"streamFileId\":1,\"md5\":\"%*32s\"},	\
										\"id\":%*d,\"message\":\"success\"}"
/*OTA下载请求，数据格式*/
#define	OTA_DOWNLOADE_REPLY				"{\"id\": \"1\",\"version\": \"1.0\",\"params\": {	\
										\"fileInfo\":{\"streamId\":%d,\"fileId\":1},\"fileBlock\":{\"size\":%d,\"offset\":%d}}}"



#define MQTT_DEBUG  0

#define	PACK_BUFF_LEN	512				//MQTT报文缓冲区长度
#define	DOWNLOADE_SIZE	256				//OTA事件中，每次下载的大小
/****************************  结构体定义 *********************************/

/*MQTT报文控制结构体*/
typedef struct{
	/*MQTT报文控制*/
	uint8_t		Pack_buff[PACK_BUFF_LEN];	//报文缓冲区
	uint16_t	MessageID;					//记录报文标识符（订阅、发布）
	uint16_t	Fixed_len;					//固定头(Fixed header)长度:表示数据包类型及数据包的分组类标识
	uint16_t	Variable_len;				//可变头(Variable header)长度:数据包类型决定了可变头是否存在及其具体内容
	uint16_t	Payload_len;				//消息体(payload)长度:客户端收到的具体内容
	uint16_t	Remaining_len;				//剩余长度 = 可变报头的长度 + 有效载荷的长度。
	
	/*MQTT报文裁剪解析缓冲区*/
	uint8_t		CMD_buff[PACK_BUFF_LEN];	//服务端发送的报文存放缓冲区，用于控制设备工作
	
	/*OTA升级控制*/
	int 		size;						//服务端推送升级包的大小
	int 		streamID;					//服务端推送升级包的ID
	uint8_t 	tempVersionNameBuff[10];	//服务端推送升级包的版本号（临时缓冲区）
	char 		pathFileName[30];			//下载到的.bin文件名
	uint16_t	counter;					//OTA事件一共下载多少次
	uint16_t	num;						//OTA事件当前下载到第几次
	uint16_t	downloadLen;				//OTA事件中本次下载的长度
	uint8_t		OTA_buff[DOWNLOADE_SIZE];	//OTA事件中，每次发布回复报文的缓冲区
}MQTT_CB;



/****************************  全局变量定义 *********************************/
extern MQTT_CB Ali_mqtt;


/****************************  函数定义 *********************************/
//公用函数
void Ali_MQTT_ConnectPack(void);
void Ali_MQTT_Received(uint8_t* data, uint16_t len);//Connect连接服务器
void Ali_MQTT_SubcribePack(char* topic);//订阅某主题
void Ali_MQTT_Publish_Deal(uint8_t* data, uint16_t len);//处理接收的发布报文
void Ali_MQTT_PublishQs0(const char* topic ,const char * data);//向某主题发送某消息（等级0）
void Ali_MQTT_PublishQs1(const char* topic ,const char * data);//向某主题发送某消息（等级1）
void OTA_DownLoad(uint16_t size, int offset);//OTA下载
void OTA_Over(void);

#endif
