/**
  ******************************************************************************
  * @file    bsp_cat1.c
  * @author  liangxu
  * @version V1.0
  * @date    2024-01-03
  * @brief   有人云cat1-4G驱动程序
  ******************************************************************************
  */ 

#include "bsp_cat1.h"
#include "stm32f10x.h"
#include <stdarg.h> 
#include <string.h>

/*****************************全局变量及操控函数**************************************/
Buff Cat1_Buff;
ConnectState Cat1_State;


/*****************************私用函数**************************************/
static void NVIC_Configuration(void);
static void Cat1_USART_Config(void);
static void Cat1_Func_GPIO_Config(void);
static void Cat1_delay_Ms(uint32_t time);

/**  缓冲区初始化 **/
void Buff_init(Buff* mybuff)
{
	mybuff->flag = 0;
	mybuff->len =0;
	memset(mybuff->buff,0,BUFFLEN);
}
/**  缓冲区写一个字节 **/
void Buff_write(Buff* mybuff,uint8_t ch)
{
	if(mybuff->len == BUFFLEN) //满了就清空，重新放
	{
		memset(mybuff->buff,0,BUFFLEN);
		mybuff->len =0;
		mybuff->flag = 0;
	}
	if(mybuff->flag ==1) return; //不可写状态则返回
	
	mybuff->buff[mybuff->len] = ch;
	mybuff->len++;
	
	if(mybuff->len == BUFFLEN)
		Buff_write_end(mybuff);//变成不可写状态，可读

}
/**  缓冲区写入完成，不可写入 **/
void Buff_write_end(Buff* mybuff)
{
	mybuff->flag =1;
}

/**  缓冲区读取案例 **/
void Buff_read(Buff* mybuff)
{
	if(mybuff->flag == 0) return;
	for(uint16_t i=0;i<mybuff->len;++i)
		Usart_SendByte(USART1,mybuff->buff[i]);
	mybuff->len=0;
	mybuff->flag=0;
}
/**  缓冲区读取完成 **/
void Buff_read_end(Buff* mybuff)
{
	mybuff->len=0;
	mybuff->flag=0;
}

/**  Cat1状态初始化 **/
void State_init(ConnectState* state)
{
	state->LinkA_TCP_ConnectFlag &= 0x00;
	state->LinkA_MQTT_ConnectFlag &= 0x00;
	state->LinkA_OTA_EventFlag &= 0x00;
}
/*****************************函数定义*************************************/
 /**
  * @brief  Cat1模组连接的USART 中断配置,私有函数
  * @param  无
  * @retval 无
  */
static void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
	
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);/* 嵌套向量中断控制器组选择 */
  NVIC_InitStructure.NVIC_IRQChannel = CAT1_USART_IRQ;/* 配置USART为中断源 */
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;/* 抢断优先级*/
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1; /* 子优先级 */
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;/* 使能中断 */
  
  NVIC_Init(&NVIC_InitStructure);/* 初始化配置NVIC */
}

/**
  * @brief  Cat1模组的Link引脚中断配置，检测引脚上拉
  * @param  无
  * @retval 无
  */
static void Link_EXTI_Config(void)
{
	
	/*配置 LINK A、B引脚，cat1模组输出，MCU的GPIO引脚输入*/
	RCC_APB2PeriphClockCmd(CAT1_LINK_CLK,ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = CAT1_LINKA_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING ; /*浮空输入*/  
	GPIO_Init(CAT1_LINKA_GPIO_PORT, &GPIO_InitStructure);	//启动GPIO
	
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel = CAT1_LINKA_INT_EXTI_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	/* 选择EXTI的信号源 */
	EXTI_InitTypeDef EXTI_InitStruct;
	GPIO_EXTILineConfig(CAT1_LINKA_PortSourceGPIOx,CAT1_LINKA_PinSourcex);/* 选择EXTI的信号源 */
	EXTI_InitStruct.EXTI_Line = CAT1_LINKA_EXTI_LINE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt; /* EXTI为中断模式 */
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising_Falling; //上下沿均触发
	EXTI_InitStruct.EXTI_LineCmd = ENABLE; 			/* 使能中断 */
	EXTI_Init(&EXTI_InitStruct);
}

/**
  * @brief  Cat1模组linkA的中断服务函数
  */
void CAT1_LINKA_IRQHandler(void)
{
	if(EXTI_GetITStatus(CAT1_LINKA_EXTI_LINE) != RESET) 
	{
		if(GPIO_ReadInputDataBit(CAT1_LINKA_GPIO_PORT,CAT1_LINKA_GPIO_PIN) == 1)//linkA输出高电平，表示连接上服务器
		{
			LED_GREEN;
			printf("\r\nTCP连接服务器成功，并发送connect报文请求\n");
			Cat1_State.LinkA_TCP_ConnectFlag |= 0x01;
			Cat1_State.LinkA_MQTT_ConnectFlag&= ~0x01;
			Cat1_State.LinkA_OTA_EventFlag&=~0x01;
			Ali_MQTT_ConnectPack();//发送连接报文
		}
		else
		{
			LED_RED;
			printf("TCP连接断开\n");
			Cat1_State.LinkA_TCP_ConnectFlag &= ~0x01;
			Cat1_State.LinkA_MQTT_ConnectFlag&= ~0x01;
			Cat1_State.LinkA_OTA_EventFlag&=~0x01;
		}
		EXTI_ClearITPendingBit(CAT1_LINKA_EXTI_LINE);     /*清除中断标志位*/
	}  
}

/**
  * @brief  Cat1模组等待linkATCP连接成功（检测高电平）
  */
uint8_t linkA_TCP_Connect(uint16_t waitMs)
{
	uint16_t counts;
	uint8_t result =0;
	do
	{
		if(1==GPIO_ReadInputDataBit(CAT1_LINKA_GPIO_PORT,CAT1_LINKA_GPIO_PIN))
		{
			result = 1;
			break;
		}

		Cat1_delay_Ms(1);
		counts++;
	}while(counts<waitMs);
	return result;
}

 /**
  * @brief  Cat1模组连接的USART GPIO 配置,工作参数配置,私有函数
  * @param  无
  * @retval 无
  */
static void Cat1_USART_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	/*开启时钟*/
	CAT1_USART_GPIO_APBxClkCmd(CAT1_USART_GPIO_CLK, ENABLE);// 打开串口GPIO的时钟
	CAT1_USART_APBxClkCmd(CAT1_USART_CLK, ENABLE);// 打开串口外设的时钟

	/*将USART Tx的GPIO配置为推挽复用模式*/ 
	GPIO_InitStructure.GPIO_Pin = CAT1_USART_TX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(CAT1_USART_TX_GPIO_PORT, &GPIO_InitStructure);

	/*将USART Rx的GPIO配置为浮空输入模式*/ 
	GPIO_InitStructure.GPIO_Pin = CAT1_USART_RX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(CAT1_USART_RX_GPIO_PORT, &GPIO_InitStructure);
	
	/*配置串口的工作参数*/ 
	USART_InitStructure.USART_BaudRate = CAT1_USART_BAUDRATE;// 配置波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;// 配置针数据字长 8
	USART_InitStructure.USART_StopBits = USART_StopBits_1;// 配置停止位 1
	USART_InitStructure.USART_Parity = USART_Parity_No ;// 配置校验位 none
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;// 配置硬件流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;// 配置工作模式，收发一起
	USART_Init(CAT1_USARTx, &USART_InitStructure);// 完成串口的初始化配置
	
	/*使能串口*/ 
	NVIC_Configuration();// 串口中断优先级配置
	USART_ITConfig(CAT1_USARTx, USART_IT_RXNE, ENABLE);	// 使能串口接收中断
	USART_ITConfig ( CAT1_USARTx, USART_IT_IDLE, ENABLE ); //使能串口总线空闲中断 
	USART_Cmd(CAT1_USARTx, ENABLE);		// 使能串口

	// 清除发送完成标志
	//USART_ClearFlag(USART1, USART_FLAG_TC);     
}

 /**
  * @brief  Cat1模组功能引脚的 GPIO 配置,私有函数
  * @param  无
  * @retval 无
  */
static void Cat1_Func_GPIO_Config(void)
{
	//启动 LINK、POWERKEY 的GPIO时钟
	RCC_APB2PeriphClockCmd( CAT1_LINK_CLK | CAT1_POWERKEY_CLK |CAT1_RESETKEY_CLK , ENABLE);
	
	/*配置 POWERKEY 引脚，cat1模组输入，MCU的GPIO引脚输出*/
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = CAT1_POWERKEY_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; /*通用推挽输出*/  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;/*引脚速率50MHz */  
	GPIO_Init(CAT1_POWERKEY_GPIO_PORT, &GPIO_InitStructure);	//启动GPIO
	
	

	
	/*功能引脚:RESET  模块重启复位*/
	GPIO_InitStructure.GPIO_Pin = CAT1_RESETKEY_GPIO_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; /*通用推挽输出*/  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;/*引脚速率50MHz */  
	GPIO_Init(CAT1_RESETKEY_GPIO_PORT, &GPIO_InitStructure);	//启动GPIO
}

 /**
  * @brief  Cat1模组初始化,公用函数
  * @param  无
  * @retval 无
  */
void Cat1_Init(void)
{
	/******************GPIO及串口初始化*****************************/
	Cat1_Func_GPIO_Config();//配置模组其他功能引脚
	Cat1_USART_Config();//配置串口及串口中断优先级
	
	/******************结构体初始化*****************************/
	Buff_init(&Cat1_Buff);//初始化缓冲区
	State_init(&Cat1_State);//状态初始化
	
	/******************开机、复位等待进入配置模式*****************************/
	CAT1_POWERKEY_ON; //开机
	do{
		CAT1_RESET;	//复位	
	}while(Cat1_Wait_Reset(30) == 0);//等待30s，进入配置模式则跳出
	
	/******************AT指令设置参数并保存*****************************/
		//Cat1_AT_Test();//测试//Cat1_Config_Exit();
	Cat1_LinkA_Connect(HOST_URL,PORT);
	Cat1_Heart_Config(60,"C000");
	Cat1_Config_Save();
	
	/*********配置link中断，等待连接成功，触发中断,发送报文*******/
	Link_EXTI_Config();
	
}

/**
  * @brief  MCU串口接收Cat1模组数据的中断服务函数
  */
void CAT1_USART_IRQHandler(void)
{
	if(USART_GetITStatus(CAT1_USARTx,USART_IT_RXNE)!=RESET)
	{	
		uint8_t ucCh  = USART_ReceiveData( CAT1_USARTx );
		Buff_write(&Cat1_Buff,ucCh );
	}	
	
	if ( USART_GetITStatus( CAT1_USARTx, USART_IT_IDLE ) == SET )                                         //数据帧接收完毕
	{	
		Buff_write_end(&Cat1_Buff);
		USART_ReceiveData( CAT1_USARTx );                                                              //由软件序列清除中断标志位(先读USART_SR，然后读USART_DR)
		
	}
}


 /**
  * @brief  MCU串口向Cat1模组发送数据,公用函数
  * @param  uint8_t 字符
  * @retval 无
  */
/*****************  发送一个字符 **********************/
void Cat1_USARTx_SendByte( uint8_t ch)
{
	USART_SendData(CAT1_USARTx,ch);/* 发送一个字节数据到USART */
	while (USART_GetFlagStatus(CAT1_USARTx, USART_FLAG_TXE) == RESET);	/* 等待发送数据寄存器为空 */
}
/*****************  发送字符串 **********************/
void Cat1_USARTx_SendString( char *str)
{
	unsigned int k=0;
	do 
	{
		Cat1_USARTx_SendByte( *(str + k) );
		k++;
	} while(*(str + k)!='\0');
  
  while(USART_GetFlagStatus(CAT1_USARTx,USART_FLAG_TC)==RESET) /* 等待发送完成 */
  {}
}

/*****************  发送一个16位数 **********************/
void Cat1_USARTx_SendHalfWord( uint16_t ch)
{
	uint8_t temp_h = (ch&0XFF00)>>8;/* 取出高八位 */
	uint8_t temp_l = ch&0XFF;/* 取出低八位 */
	
	Cat1_USARTx_SendByte(temp_h);	/* 发送高八位 */
	while (USART_GetFlagStatus(CAT1_USARTx, USART_FLAG_TXE) == RESET);
	Cat1_USARTx_SendByte(temp_l);	/* 发送低八位 */
	while (USART_GetFlagStatus(CAT1_USARTx, USART_FLAG_TXE) == RESET);	
}

/*****************  发送自由拼接字符串 **********************/
#define PRINT_BUFF_LEN 256
static char PrintBuff[PRINT_BUFF_LEN] = {0};//私有缓冲区
void Cat1_USARTx_printf( const char* fmt, ...)
{
	va_list args;
	uint16_t sumLen;
	
	va_start(args, fmt);
	sumLen = vsnprintf(PrintBuff, sizeof(PrintBuff), fmt, args);
	va_end(args);
	
	if(sumLen + 1 > PRINT_BUFF_LEN)
	{
		printf("要写入的长度%d大于读写缓冲区长度！写入失败\n;",sumLen);
		return;
	}
	else
		PrintBuff[sumLen] = '\0';
	
	//printf("数据发送为%s\n",PrintBuff);
	Cat1_USARTx_SendString(PrintBuff);
}

/*****************  发送自由拼接的数据 **********************/


 /**
  * @brief  Cat1模组通过串口2向MCU发送数据,公用函数
  * @param  data数据，len长度
  * @retval 无
  */
void Cat1_Rec_Handle(uint8_t* data,uint16_t len)
{
	for(uint16_t i = 0; i<len; ++i)
		USART_SendData(DEBUG_USARTx, data[i]);
	
	if((len == 10)&&(memcmp(data,"WH-LTE-7S1",10)))
	{
		printf("WH-LTE-7S1复位成功,等待连接服务器\n");
		Link_EXTI_Config();/*配置link中断，等待连接成功，触发中断*/
		
	}
}

/**
  * @brief  Cat1模组延迟函数(去耦合)
  * @param  time等待时间（ms）
  */
static void Cat1_delay_Ms(uint32_t time)
{
	SysTick_Delay_Ms(time);
}


 /**
  * @brief  Cat1模组等待复位成功，并进入配置状态
  * @param  waitTime等待时间（s）
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_Wait_Reset(uint8_t waitTime)
{
	uint8_t result = 0;
	uint32_t count = 0; //ms数
	while(count/1000 < waitTime)
	{
		if(Cat1_Buff.flag == 1)//可读状态（存在一组数据）
		{
			if((Cat1_Buff.len == 10) && (memcmp(Cat1_Buff.buff,"WH-LTE-7S1",10) == 0))
			{
				printf("\r\n4G模块复位成功,准备退出透传\r\n");
				Cat1_USARTx_SendString("+++");
			}
			else if((Cat1_Buff.len == 1) && (memcmp(Cat1_Buff.buff,"a",1) == 0))
				Cat1_USARTx_SendByte('a');
			else if((Cat1_Buff.len == 5) && (memcmp(Cat1_Buff.buff,"+ok\r\n",5) == 0) )
			{
				printf("4G模块进入配置模式\r\n");
				Buff_read_end(&Cat1_Buff);
				result = 1; 
				break;
			}
			Buff_read_end(&Cat1_Buff);
		}
		
		Cat1_delay_Ms(10);
		count+=10;
	}
	

	return result;
}

 /**
  * @brief  Cat1模组等待回复
  * @param  data回复数据，dataLen比较数据长度,timeoutMs超时时间(ms)
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_Wait_Recv(const char* data, uint8_t dataLen, uint32_t timeoutMs)
{
	uint8_t result = 0;
	uint32_t count = 0;
	do{
		if(Cat1_Buff.flag == 1)
		{
			if((Cat1_Buff.len == dataLen) && (memcmp(Cat1_Buff.buff,data,dataLen) == 0) )
			{
				//printf("%s\n",Cat1_Buff.buff);
				result = 1;
				Buff_read_end(&Cat1_Buff);
				break;
			}
			else
			{
				//printf("%s\n",Cat1_Buff.buff);
			}
			Buff_read_end(&Cat1_Buff);
		}
		
		Cat1_delay_Ms(1);
		
	}while(++count <timeoutMs );
	//printf("%d\n",count);
	return result;
}

 /**
  * @brief  Cat1AT测试指令
  * @param  连接服务器IP,端口号
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_AT_Test(void)
{
	uint8_t result = 0;
	Cat1_USARTx_printf("AT\r\n");
	if(Cat1_Wait_Recv("\r\nOK\r\n",6,100) == 0)
	{printf("AT指令测试失败\n"); result = 0;}
	else
	{printf("AT指令测试成功\n"); result = 1;}	
	return result;
}

 /**
  * @brief  Cat1模组使用LINKA配置TCP连接并启动
  * @param  连接服务器IP,端口号
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_LinkA_Connect(const char* IP, int port)
{
	uint8_t result = 0;
	
	Cat1_USARTx_printf("AT+SOCKA=TCP,%s,%d\r\n",IP,port);
	if(Cat1_Wait_Recv("\r\nOK\r\n",6,100) == 0)
	{printf("配置TCP连接失败\n");return result;}
		
	Cat1_USARTx_printf("AT+SOCKBEN=OFF\r\n");
	if(Cat1_Wait_Recv("\r\nOK\r\n",6,100) == 0)
	{printf("配置关闭LinkB失败\n");return result;}
	
	Cat1_USARTx_printf("AT+SOCKAEN=ON\r\n");
	if(Cat1_Wait_Recv("\r\nOK\r\n",6,200) == 0)
	{printf("配置启动LinkA失败\n");return result;}
	
	printf("LINKA配置成功\n");
	result = 1;
	return result;
}

/**
  * @brief  Cat1模组保存配置
  * @param  无
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_Config_Save(void)
{
	uint8_t result = 0;
	
	//发送指令
	Cat1_USARTx_printf("AT+S\r\n");
	if(Cat1_Wait_Recv("\r\nOK\r\n",6,500) == 0)
	{printf("保存配置失败\n");return result;}
	else
	{printf("保存配置成功,等待重启\n");result = 1;}
	
	if(Cat1_Wait_Recv("WH-LTE-7S1", 10, 20000) == 1)
	{printf("重启成功\n");}

	return result;
}


/**
  * @brief  Cat1模组心跳配置
  * @param  cycleSeconds心跳周期（s）,自定义心跳符号
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_Heart_Config(uint16_t cycleSeconds, const char* symbol)
{
	uint8_t result = 0;
	Cat1_USARTx_printf("AT+HEART=ON,NET,USER,%d,%s\r\n",cycleSeconds,symbol);
	if(Cat1_Wait_Recv("\r\nOK\r\n",6,100) == 0)
	{printf("配置心跳失败\n");return result;}
	else
	{printf("配置心跳成功\n");result = 1;}
	
	return result;
}

/**
  * @brief  Cat1进入退出配置模式
  * @param  
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_Config_Exit(void)
{
	uint8_t result = 0;
	Cat1_USARTx_printf("AT+ENTM\r\n");
	if(Cat1_Wait_Recv("\r\nOK\r\n",6,1000) == 0)
	{printf("退出配置模式失败\n");return result;}
	else
	{printf("退出配置模式成功,进入透传模式\n");result = 1;}
	return result;
}

/**
  * @brief  Cat1重启
  * @param  
  * @retval 0失败 ，1成功
  */
uint8_t Cat1_Reset(void)
{
	Cat1_USARTx_printf("AT+Z\r\n");//重启
	if(Cat1_Wait_Recv("WH-LTE-7S1", 10, 3000) == 0)
	{printf("重启失败\n"); return 0;}
	else
	{printf("重启成功\n"); return 1;}
}
