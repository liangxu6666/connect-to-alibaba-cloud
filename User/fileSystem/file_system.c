/**
  ******************************************************************************
  * @file    file_system.c
  * @author  liang
  * @version V1.0
  * @date    2023-12-18
  * @brief   建立文件系统，实现在SD卡上的.bin文件传输、写入、读取
  ******************************************************************************
  */ 

#include "file_system.h"

FATFS fs;					/* FatFs文件系统对象 */
FIL fnew;					/* 文件对象 */
FRESULT res_sd;             /* 文件操作结果 */
UINT fnum;            		/* 文件成功读写数量 */
DIR dir;					/* 文件夹（路径） */
FILINFO fno;

/*
 * 函数名：file_system_init
 * 描述  ：判断有无文件系统，无则初始化创建ini文件，有则返回成功标志
 * 输入  ：无
 * 输出  ：	0：格式化失败，驱动问题
			1：文件系统不存在，格式化成功
			2：文件系统已经存在
 */
uint8_t file_system_init(void)
{
	res_sd = f_mount(&fs,"0:",1);/* 挂载SD卡，得到文件操作结果 */
	if(res_sd == FR_NO_FILESYSTEM)/* 如果没有文件系统就格式化创建创建文件系统 */
	{
		printf("SD卡还没有文件系统，即将进行格式化...\r\n");
		res_sd=f_mkfs("0:",0,0);/* 格式化 */
		if(res_sd == FR_OK)
		{
			printf("SD卡已成功格式化文件系统。\r\n");
			res_sd = f_mount(NULL,"0:",1);	 /* 格式化后，先取消挂载 */		
			res_sd = f_mount(&fs,"0:",1);	/*重新挂载*/
			return 1;
		}
		else
		{
			LED_RED;
			printf("格式化失败\r\n");
			return 0;
		}
	}
	else if(res_sd!=FR_OK){
		LED_RED;
		printf("SD卡挂载文件系统失败(%d),可能原因：SD卡初始化不成功。",res_sd);
		return 0;
	}
	else
	{
		printf("存在文件系统\n");
		return 2;
	}
}

/*
 * 函数名：update_flie_list
 * 描述  ：列举所有的文件
 * 输入  ：dirPathName文件夹路径
 * 输出  ：成功返回文件夹内个数
 */
uint16_t update_flie_list(const char* dirPathName)
{
	uint16_t fileNum = 0;
	FRESULT res = f_opendir (&dir, dirPathName);
	if(FR_OK == res)
	{
		 while (1)
		 {
			res = f_readdir(&dir, &fno);//将dir中文件信息依次放到fno中
			if(res != FR_OK || fno.fname[0] == 0)
			{
				break;//遍历完成（或出现错误）
			}
			else
			{
				printf("%s中第 %d 个文件为：%s\n",dirPathName,fileNum++,fno.fname);
			}
		 }
	}
	else
	{
		res = f_mkdir(dirPathName);
		printf("已创建%s文件夹，目前文件夹内为空\n",dirPathName);
	}
	f_closedir(&dir);
	return fileNum;
}

/*
 * 函数名：flie_bin_create
 * 描述  ：创建.bin二进制文件,首次创建
 * 输入  ：0成功
 * 输出  ：无
 */
uint8_t flie_bin_create(const char* filePathName)
{
	FRESULT res = f_open(&fnew, filePathName, FA_CREATE_ALWAYS );//无论文件是否存在，都创建新文件并覆盖现有文件。如果文件已存在，会删除原有文件内容。
	if(res != FR_OK)	{printf("文件创建失败，失败原因：(%d)\n",res); }
	else {f_close(&fnew); }
	return res;
}

/*
 * 函数名：flie_bin_read
 * 描述  ：读取.bin二进制文件，无文件返回失败，读取成功则返回成功标志
 * 输入  ：1文件路径/文件,2读取起始位置，3读取长度，4读取存放到的地址
 * 输出  ：return 读取成功标志，成功：返回读取长度（字节），失败返回-1
 */
int flie_bin_read(const char* filePathName, uint32_t startPos,uint16_t readLen,uint8_t* Readbuff)
{
	FRESULT res = f_open(&fnew, filePathName, FA_READ);//打开文件（只读）
	if (res != FR_OK) {printf("打开 %s 文件失败\n", filePathName);return -1;}
	
	res = f_lseek(&fnew,startPos);//移到对应位置
	if (res != FR_OK) 
	{printf("此文件的 %d 位置没有数据\n", startPos);f_close(&fnew);return -2;}
		
	res = f_read(&fnew, Readbuff, readLen, &fnum);		//读取文件
	f_close(&fnew);//关闭文件
	
	//校验内容
	if((res==FR_OK)  && (fnum == readLen)) 		{printf("读取成功\n");}
	else if((res==FR_OK)  && (fnum != readLen)) 	{printf("读取长度不符,读到字节数据：%d\n",fnum);}
	else 											{printf("文件读取失败，失败原因：(%d)\n",res_sd);return -1;}
	return fnum;
}


/*
 * 函数名：flie_bin_write
 * 描述  ：写入.bin二进制文件，无文件返回失败，读取成功则返回成功标志
 * 输入  ：1文件路径/文件,2起始位置，3写长度，4缓冲区
 * 输出  ：无
 */
void flie_bin_write(const char* filePathName,uint16_t writeLen,uint8_t* writebuff)
{
	FRESULT res = f_open(&fnew, filePathName, FA_WRITE);//打开文件
	if(res != FR_OK)	{printf("文件打开失败，失败原因：(%d)\n",res); return;}
	
	res_sd = f_lseek(&fnew, f_size(&fnew));//定位到最后
	if(res!=FR_OK) 	{printf("文件定位失败，失败原因：(%d)\n",res); f_close(&fnew); return;}
	
	res_sd = f_write(&fnew,writebuff,writeLen,&fnum);
	if(res!=FR_OK) {printf("文件写入失败，失败原因：(%d)\n",res); f_close(&fnew); return;}
	
	f_close(&fnew);
}


/*
 * 函数名：composite_string
 * 描述  ：添加字符串至buff最后
 * 输入  ：buff缓冲区，suffix后缀
 * 输出  ：buff当前长度
 */
uint16_t composite_string(char* buff, const char* suffix)
{
	uint16_t index = 0, i=0;
	while(buff[index]!='\0')
		++index;
	
	while(suffix[i]!='\0')
	{
		buff[index++] = suffix[i++];
	}
	buff[index] = '\0';
	return index;
}


/*
 * 函数名：get_remainder
 * 描述  ：获取文件完整读写次数
 * 输入  filePathName文件路径名，factor因子
 * 输出  ：次数
 */
int get_divisor(const char* filePathName , uint16_t factor)
{
	FRESULT res = f_open(&fnew, filePathName, FA_WRITE);//打开文件
	if(res != FR_OK)	{printf("文件打开失败，失败原因：(%d)\n",res); return -1;}
	
	int result = f_size(&fnew) / factor;
	f_close(&fnew);
	return result;
}

/*
 * 函数名：get_remainder
 * 描述  ：获取文件完整后，剩余部分的大小
 * 输入  ：
 * 输出  ：
 */
int get_remainder(const char* filePathName , uint16_t factor)
{
	FRESULT res = f_open(&fnew, filePathName, FA_WRITE);//打开文件
	if(res != FR_OK)	{printf("文件打开失败，失败原因：(%d)\n",res); return -1;}
	
	int result = f_size(&fnew) % factor;
	f_close(&fnew);
	return result;
}
