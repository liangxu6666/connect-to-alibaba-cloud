#ifndef __FILE_SYSTEM_H
#define __FILE_SYSTEM_H

#include "ff.h"
#include "stm32f10x.h" 
#include "bsp_usart.h"
#include "bsp_led.h" 



extern FATFS fs;					/* FatFs文件系统对象 */
extern FIL fnew;					/* 文件对象 */
extern FRESULT res_sd;             /* 文件操作结果 */
extern UINT fnum;            		/* 文件成功读写数量 */
extern DIR dir;					/* 文件夹（路径） */
extern FILINFO fno;


//共用函数
uint8_t file_system_init(void);
uint16_t update_flie_list(const char* dirPathName);
uint8_t flie_bin_create(const char* filePathName);
int flie_bin_read(const char* filePathName, uint32_t startPos,uint16_t readLen,uint8_t* Readbuff);
void flie_bin_write(const char* filePathName,uint16_t writeLen,uint8_t* writebuff);
uint16_t composite_string(char* buff, const char* suffix);
int get_divisor(const char* filePathName , uint16_t factor);
int get_remainder(const char* filePathName , uint16_t factor);




























#endif
