/**
  ******************************************************************************
  * @file    main.c
  * @author  liang
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   Cat1模组测试
  ******************************************************************************
  * @attention
  *
  * 实验平台: F103 STM32 开发板 
  *
  ******************************************************************************
  */ 
 
 
#include "stm32f10x.h"
#include "./led/bsp_led.h" 
#include "./usart/bsp_usart.h" 
#include "bsp_cat1.h"
#include "rx_data_queue.h"
#include "mqtt.h"
#include "bsp_TiMbase.h"
#include "ota.h"
#include "file_system.h"


static void Event1(uint8_t* data,uint16_t len);
static void Event2(uint8_t* data,uint16_t len);
static void test(void);



/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
int main(void)
{	
	
	
	LED_GPIO_Config();/* 初始化RGB彩灯 */
	USART_Config();/* 初始化USART 配置模式为 115200 8-N-1 */
	rx_queue_init();QUEUE_DATA_TYPE *rx_data;//读取串口队列的指针
	
	file_system_init();
	test();
	
	Cat1_Init();			//初始化并进入透传
	BASIC_TIM_Init();
	
	/******************等待云端回复报文，确认MQTT连接成功****************/
	printf("进入DeBug串口-->Cat1的透传模式\n");
	while(1)
	{
		
		//接收串口缓冲区数据,并处理
		rx_data = cbRead(&rx_queue);
		if(rx_data!=NULL)
		{
			Event1((uint8_t*)rx_data->head, rx_data->len);
			cbReadFinish(&rx_queue);
		}
		
		//接收串口2缓冲区数据,并处理
		if(Cat1_Buff.flag == 1)
		{
			Event2(Cat1_Buff.buff,Cat1_Buff.len);
			Buff_read_end(&Cat1_Buff);
		}
		
		
		
	}
	return 0;
}

static void Event1(uint8_t* data,uint16_t len)
{
	//printf("串口1接收到%d个数据\n",len);
	for(uint16_t i = 0; i<len; ++i)
		Cat1_USARTx_SendByte(data[i]);
}
static void Event2(uint8_t* data,uint16_t len)
{
	//printf("串口2接收到%d个数据\n",len);
	
	if((len == 1)&&(data[0] == 'a' ))
		{ Cat1_USARTx_SendByte('a'); printf("退出透传模式 \n");}
	else
		Ali_MQTT_Received(data,len);
}

static void test(void)
{
	//SysTick_Delay_Ms(3000);Ali_MQTT_ConnectPack();while(1);
	//update_flie_list(UPDATE_DIR_PATH);
//	if(0==flie_bin_create("0:/VERSION/V2401161.bin"))
//			printf("创建文件\n");//将之后的数据放入此文件中
//	while(1);
}
/*********************************************END OF FILE**********************/
