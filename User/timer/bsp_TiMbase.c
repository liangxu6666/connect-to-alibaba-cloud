
// 基本定时器TIMx,x[6,7]定时初始化函数

#include "bsp_TiMbase.h" 
#include "mqtt.h"
#include "bsp_cat1.h"

volatile uint32_t time = 0; // ms 计时变量 


// 中断优先级配置
static void BASIC_TIM_NVIC_Config(void)
{
    NVIC_InitTypeDef NVIC_InitStructure; 
    // 设置中断组为0
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);		
		// 设置中断来源
    NVIC_InitStructure.NVIC_IRQChannel = BASIC_TIM_IRQ ;	
		// 设置主优先级为 0
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	 
	  // 设置抢占优先级为3
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/*
 * 注意：TIM_TimeBaseInitTypeDef结构体里面有5个成员，TIM6和TIM7的寄存器里面只有
 * TIM_Prescaler和TIM_Period，所以使用TIM6和TIM7的时候只需初始化这两个成员即可，
 * 另外三个成员是通用定时器和高级定时器才有.
 *-----------------------------------------------------------------------------
 *typedef struct
 *{ TIM_Prescaler            都有
 *	TIM_CounterMode			     TIMx,x[6,7]没有，其他都有
 *  TIM_Period               都有
 *  TIM_ClockDivision        TIMx,x[6,7]没有，其他都有
 *  TIM_RepetitionCounter    TIMx,x[1,8,15,16,17]才有
 *}TIM_TimeBaseInitTypeDef; 
 *-----------------------------------------------------------------------------
 */


static void BASIC_TIM_Mode_Config(void)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    BASIC_TIM_APBxClock_FUN(BASIC_TIM_CLK, ENABLE);	// 开启定时器时钟,即内部时钟CK_INT=72M
    TIM_TimeBaseStructure.TIM_Period = BASIC_TIM_Period;	// 自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
    TIM_TimeBaseStructure.TIM_Prescaler= BASIC_TIM_Prescaler; // 时钟预分频数为
	
    //TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;// 时钟分频因子 ，基本定时器没有，不用管
    //TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; // 计数器计数模式，基本定时器只能向上计数，没有计数模式的设置
	//TIM_TimeBaseStructure.TIM_RepetitionCounter=0;// 重复计数器的值，基本定时器没有，不用管
	 
    TIM_TimeBaseInit(BASIC_TIM, &TIM_TimeBaseStructure); // 初始化定时器
    TIM_ClearFlag(BASIC_TIM, TIM_FLAG_Update);// 清除计数器中断标志位
    TIM_ITConfig(BASIC_TIM,TIM_IT_Update,ENABLE);// 开启计数器中断
    TIM_Cmd(BASIC_TIM, ENABLE);		// 使能计数器
}

void BASIC_TIM_Init(void)
{
	BASIC_TIM_NVIC_Config();
	BASIC_TIM_Mode_Config();
}

void  BASIC_TIM_IRQHandler (void)
{
	if ( TIM_GetITStatus( BASIC_TIM, TIM_IT_Update) != RESET ) 
	{	
		
		
		if(time++ >= 60000) /* 600000 * 1 ms = 600s 时间到 */
		{
			time = 0;
			if((Cat1_State.LinkA_MQTT_ConnectFlag & 0x01) )	//处于MQTT透传阶段
			{
				if ((Cat1_State.LinkA_OTA_EventFlag & 0x01) == 0)//且不处于OTA升级阶段
				{
					Ali_MQTT_PublishQs0(DEVICE_PROPERTY_POST_TOPIC,"{\"params\":{\"temperature\":15.21,\"mlux\":40.21,\"Humidity\":90.01}}");
					printf("发送数据报文\n");
				}
			}
		}
		
		TIM_ClearITPendingBit(BASIC_TIM , TIM_FLAG_Update);  		 
	}		 	
}


/*********************************************END OF FILE**********************/
